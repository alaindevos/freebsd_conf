/usr/bin/logger "x shell"      # log
export PATH=/sbin:/bin:/usr/sbin:/usr/bin:/usr/local/sbin:/usr/local/bin:/home/x/bin
export PATH=/home/x/.local/bin:$PATH # PYTHON PIP (x)
export PATH=/home/x/.rvm/bin:$PATH   # RUBY
export PATH=/home/x/go/bin:$PATH     # GO
export PATH=/home/x/.cargo/bin:$PATH # RUST
export PATH=/home/x/perl5/bin:$PATH  # PERL
export PATH=/sbin:/bin:/usr/sbin:/usr/bin:/usr/local/sbin:/usr/local/bin:/home/x/bin:$PATH
export PATH=/home/x/opam:$PATH
export PATH=/home/x/DCD/bin:$PATH
export PATH=/home/x/serve-d:$PATH
export PATH=/home/x/.local/share/gem/ruby/3.0/bin:$PATH
export PERL_LOCAL_LIB_ROOT=/home/x/perl5
export PERL5LIB=/home/x/perl5/lib/perl5
export PERL_MB_OPT="--install_base \"/home/x/perl5\""
export PERL_MM_OPT="INSTALL_BASE=/home/x/perl5"
#export ; Lang & Locale ; nl : base language ; BE : Country territory ; UTF-8 codeset , characterset encoding  ; LOCALE==LANG
export LANG="nl_BE.UTF-8"
export LANGUAGE="nl_BE.UTF-8"
export LC_ALL=$LANG
# CTYPE : character set classification
export LC_CTYPE=$LANG
export LC_NUMERIC=$LANG
export LC_TIME=$LANG
# Collation order (sort)
export LC_COLLATE=$LANG
export LC_MONETARY=$LANG
export LC_MESSAGES=$LANG
export LC_PAPER=$LANG
export LC_NAME=$LANG
export LC_ADDRESS=$LANG
export LC_TELEPHONE=$LANG
export LC_MEASUREMENT=$LANG
export LC_IDENTIFICATION=$LANG
#export
export MM_CHARSET=$LANG
export TZ=Europe/Brussels
#export
export CLICOLOR
export COLORFGBG="15;0"
export COLORTERM="truecolor"
export LS_COLORS="rs=0:di=01;34:ln=01;36:mh=00:pi=40;33:so=01;35:do=01;35:bd=40;33;01:cd=40;33;01:or=40;31;01:mi=00:su=37;41:sg=30;43:ca=30;41:tw=30;42:ow=34;42:st=37;44:ex=01;32:*.tar=01;31:*.tgz=01;31:*.arc=01;31:*.arj=01;31:*.taz=01;31:*.lha=01;31:*.lz4=01;31:*.lzh=01;31:*.lzma=01;31:*.tlz=01;31:*.txz=01;31:*.tzo=01;31:*.t7z=01;31:*.zip=01;31:*.z=01;31:*.dz=01;31:*.gz=01;31:*.lrz=01;31:*.lz=01;31:*.lzo=01;31:*.xz=01;31:*.zst=01;31:*.tzst=01;31:*.bz2=01;31:*.bz=01;31:*.tbz=01;31:*.tbz2=01;31:*.tz=01;31:*.deb=01;31:*.rpm=01;31:*.jar=01;31:*.war=01;31:*.ear=01;31:*.sar=01;31:*.rar=01;31:*.alz=01;31:*.ace=01;31:*.zoo=01;31:*.cpio=01;31:*.7z=01;31:*.rz=01;31:*.cab=01;31:*.wim=01;31:*.swm=01;31:*.dwm=01;31:*.esd=01;31:*.jpg=01;35:*.jpeg=01;35:*.mjpg=01;35:*.mjpeg=01;35:*.gif=01;35:*.bmp=01;35:*.pbm=01;35:*.pgm=01;35:*.ppm=01;35:*.tga=01;35:*.xbm=01;35:*.xpm=01;35:*.tif=01;35:*.tiff=01;35:*.png=01;35:*.svg=01;35:*.svgz=01;35:*.mng=01;35:*.pcx=01;35:*.mov=01;35:*.mpg=01;35:*.mpeg=01;35:*.m2v=01;35:*.mkv=01;35:*.webm=01;35:*.webp=01;35:*.ogm=01;35:*.mp4=01;35:*.m4v=01;35:*.mp4v=01;35:*.vob=01;35:*.qt=01;35:*.nuv=01;35:*.wmv=01;35:*.asf=01;35:*.rm=01;35:*.rmvb=01;35:*.flc=01;35:*.avi=01;35:*.fli=01;35:*.flv=01;35:*.gl=01;35:*.dl=01;35:*.xcf=01;35:*.xwd=01;35:*.yuv=01;35:*.cgm=01;35:*.emf=01;35:*.ogv=01;35:*.ogx=01;35:*.aac=00;36:*.au=00;36:*.flac=00;36:*.m4a=00;36:*.mid=00;36:*.midi=00;36:*.mka=00;36:*.mp3=00;36:*.mpc=00;36:*.ogg=00;36:*.ra=00;36:*.wav=00;36:*.oga=00;36:*.opus=00;36:*.spx=00;36:*.xspf=00;36:"
#export LSCOLORS="cEgxcxdxbxegedabagacad"
#export GCC_COLORS='error=01;31:warning=01;35:note=01;36:caret=01;32:locus=01:quote=01'
#export EXA_COLORS="di=35"
#export
export PAGER=less
export EDITOR=nvim
export VISUAL=nvim
export TERM="xterm-256color"
export XDG_RUNTIME_DIR=/home/x/TMP
export XKB_DEFAULT_LAYOUT="be(nodeadkeys)"
export XKB_DEFAULT_RULES=evdev
export QT_QPA_PLATFORMTHEME="qt5ct"
#export WM=labwc
#export GDK_BACKEND=wayland
#export XDG_SESSION_TYPE=wayland
#export QT_QPA_PLATFORM=wayland
#export SDL_VIDEODRIVER=wayland
#export CLUTTER_BACKEND=wayland
#export BEMENU_BACKEND=wayland
#export QT_WAYLAND_DISABLE_WINDOWDECORATION=0
#export MOZ_ENABLE_WAYLAND=1
#export
export SAGE_ROOT=/usr/local
export SAGE_LOCAL=/usr/local
#export
export CPATH=/usr/local/include
export LIBRARY_PATH=/usr/local/lib
#export
export JAVA_VERSION=11
export JAVA_HOME="/usr/local/openjdk11"
export PATH=$JAVA_HOME/bin:$PATH
export BROWSER=firefox
export LD_PRELOAD=
#export dlang
export DC=ldc

source /home/x/Conf/zshhighlight/zsh-syntax-highlighting/zsh-syntax-highlighting.zsh
# opam configuration
test -r /usr/home/x/.opam/opam-init/init.zsh && . /usr/home/x/.opam/opam-init/init.zsh > /dev/null 2> /dev/null || true
# Color of *
ZSH_HIGHLIGHT_STYLES[globbing]=none


HISTFILE=~/History/zsh_histfile  # Where to save history to disk
HISTSIZE=1000000      # How many lines of history to keep in memory
SAVEHIST=1000000      # Number of history entries to save to disk
setopt APPEND_HISTORY     # Append history to the history file (no overwriting)
setopt SHARE_HISTORY      # Share history across terminals
setopt INC_APPEND_HISTORY # Immediately append to the history file, not just when a term is killed
setopt AUTO_CD            # no cd
setopt AUTO_PUSHD         # To use "push" "popd"
setopt PUSHD_IGNORE_DUPS  # To have "dirs" clean
setopt INTERACTIVE_COMMENTS # Allow comments

set -o vi
bindkey -v            # Use vi escape keystrokes
### -U , do not alias expansion , -z : zstyle
autoload -Uz colors && colors # defines several associative arrays and variables.
autoload -Uz promptinit && promptinit # Advanced prompt support
prompt off            # disable default prompt
setopt PROMPT_SUBST   # Allow custom prompt
#RPROMPT="$?"
PROMPT="$fg[green]HOST:%n: $fg[default]%d >"
# Double tab is completion mode
autoload -Uz compinit
compinit
# Give more feedback
zstyle ':completion:*' format %d
#Trigger history expansion using space
bindkey ' ' magic-space
echo -e -n "\e[3 q"   # Blinking underline cursor
tabs -4
fortune

############################################################################################
# opens any file with the correct program, e.g. e test.doc opens. Install libexo , xdg-utils.
function eee() {
	exo-open "$@" &
}   # with exo

#
function xxx() {
	xdg-open "$@" &
}   # with xdg-utils

#cs= cd-ls , go to directory and list contents
function cs {
cd "$@" && exa
}

############################################################################################
### open these extensions
alias -s {txt,wri}=geany
alias -s config=leafpad
alias -s {png,jpg,jpeg}=lximage-qt
alias -s {pdf,PDF}=atril
alias -s d=bat

alias d='dirs -v | head -10'
alias 1='cd -'
alias 2='cd -2'
alias 3='cd -3'
alias 4='cd -4'
alias 5='cd -5'
alias 6='cd -6'
alias ...='cd ../..'
alias ..='cd ..'
alias lessr='less -x4 -R '
alias less='less -x4 '
alias aa='exa -F'
alias zz='gnuls -lF --color '
alias ls=''
alias vim=nvim
alias vi=nvim
alias nv=nvim-qt
############################################################################################
[ -f ~/.fzf.zsh ] && source ~/.fzf.zsh

export CCACHE_DIR=/ccache
