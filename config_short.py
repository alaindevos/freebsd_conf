#config.set("colors.webpage.darkmode.enabled",True)
config.bind('P','hint links spawn smplayer {hint-url}')
config.bind('D','hint links spawn st -e youtube-dl {hint-url}')
config.bind('S','config-cycle statusbar.show always never')
config.bind('T','config-cycle tabs.show always never')
config.bind('A','set-cmd-text -s :open -t')
config.bind('Q','set-cmd-text -s :tab-close')
config.bind('Z','set-cmd-text -s :back')
c.url.searchengines['DEFAULT']='https://duckduckgo.com/q={}'
c.url.searchengines['W']='https://duckduckgo.com/q={}'
c.url.searchengines['X']='https://www.reddit.com/r/{}'
c.url.searchengines['C']='https://www.youtube.com/results?search_query={}'
c.downloads.location.directory = "/home/x/Qutebrowser"
c.fonts.tabs.selected='20pt default_family'
c.fonts.tabs.unselected='15pt default_family'
config.load_autoconfig()
